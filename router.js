const Authentication = require('./controllers/authentication');
const passport = require('passport');
require('./services/passport')(passport);

const requireAuth = passport.authenticate('jwt', { session: false }); // session: false, don't use cookies
const requireSignin = passport.authenticate('local', { session: false });

module.exports = function(app) {
  app.get('/', requireAuth, function(req, res) {
    res.send({ hi: 'there' });
  });
  app.post('/signin', requireSignin, Authentication.signin);
  app.post('/signup', Authentication.signup);
}