const passport = require('passport');
const User = require('../models/user');
const config = require('../config');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const LocalStratgy = require('passport-local');

// setup options for JWT Strategy
const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromHeader('authorization'),
  secretOrKey: config.secret // need to supply secret to decode
}; // where to look on requests for the token, look on header for authorization

module.exports = (passport) => {

  // password field will happen automatillcay, need to specify usernameField
  const localLogin = new LocalStratgy({ usernameField: 'email'}, function(email, password, done) {
    // Verify this username and password, call done with the user
    User.findOne({ email: email }, function(err, user) {
      if (err) { return done(err); }
      if (!user) { return done(null, false); }

      user.comparePassword(password, function(err, isMatch) {
        if (err) { return done(err); }
        if (!isMatch) { return done(null, false); }

        return done(null, user);
      });
    });
  });

  // Create JWT strategy
  const jwtLogin = new JwtStrategy(jwtOptions, function(payload, done) {
    // See if the user ID in the payload exists in our database
    // If it does, call 'done' with that other
    // otherwise, call done without a user object
    User.findById(payload.sub, function(err, user) {
      if (err) { return done(err, false); }
  
      if (user) {
        done(null, user);
      } else {
        done(null, false);
      }
    });
  });
  
  // Tell passport to use this strategy
  passport.use(jwtLogin);
  passport.use(localLogin);
}

